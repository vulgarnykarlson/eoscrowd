const def = 100000;
const gas = {};

var handler = {
    get: function(target, name) {
        return gas.hasOwnProperty(name) ? gas[name]:def;
    }
};
module.exports = new Proxy({}, handler)
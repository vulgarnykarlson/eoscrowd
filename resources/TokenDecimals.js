const defaultdec = 18;
const decimals = {
    AION    :   8,
    ADX     :   4,
    BQX     :   8,
    ENG     :   8,
    FUN     :   8,
    GTO     :   5,
    MCO     :   8,
    POE     :   8,
    POWR    :   6,
    SALT    :   8,
    SUB     :   2,
    TRX     :   6
};

var handler = {
    get: function(target, name) {
        return decimals.hasOwnProperty(name) ? decimals[name]:defaultdec;
    }
};

module.exports = new Proxy({}, handler);
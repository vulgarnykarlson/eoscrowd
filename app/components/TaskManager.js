const BinanceREST = require('../../exchangesAPI/BinanceREST');
const EventEmitter = require('events');
const TaskList = require('./TaskList');

// const exchanges = {
//     "binance": require('../../exchangesAPI/BinanceREST'),
//     "other": ""
// }

class TaskManager extends EventEmitter{
    constructor() {
        super();
        this.tasks = new TaskList(__dirname+'/taskmanager.json');
        setInterval(this.checkDeposits.bind(this), 10000);
    }

    async checkDeposits(){
        let deposits = await BinanceREST.client.depositHistory();
        if(!deposits["success"]) return false;
        const deposit = deposits["depositList"].find( (val) => {
            return this.tasks.has(val["txId"])
        });
        if(!deposit || !deposit.status) return false;
        const hash = deposit["txId"];
        this.tasks.setProp(deposit["txId"], "quantity", deposit["amount"].toFixed(2));
        let order = this.tasks.get(hash);
        let response = await BinanceREST.client.order({
            price: order.price,
            side: order.side,
            symbol:order.symbol,
            quantity:order.quantity,
        });
        this.emit("logger.debug", "Задача на выставление ордера "+this.tasks.getProp(hash, 'symbol')
            +" ; type: "+this.tasks.getProp(hash, 'side')
            +" ; price: "+this.tasks.getProp(hash, 'price')
            +" ; quantity:"+this.tasks.getProp(hash, 'quantity')
            +" выполнена");
        this.tasks.setProp(deposit["txId"], "orderId", response.orderId);
        this.tasks.setProp(deposit["txId"], "made", true);
        console.log("logger.debug", JSON.stringify(response));
    }
}
module.exports = new TaskManager;
const unix = require('../../libs/unix');
const hours = 3600;
const startTime = 1498914000; // Время начала сейла, хардкод
const BigNumber = require('bignumber.js');
const eosSupply = new BigNumber(2000000); // Количество токенов продаваемых за раунд
const EOSSale = require('../../blockchain/web3/Tokens/EOSSale');
const config = require('../../config');
const botCfg = config.bot;

// Получение текущего раунда
const getToday = module.exports.getToday = () => {
    return module.exports.getPeriod(unix.now);
};

module.exports.getPeriod = (unixtime) => {
    return Math.floor((unixtime - startTime)/23/hours + 1);
};

module.exports.periodTime = 23*hours;

// Unix время конца указанного раунда
module.exports.periodEnd = (period) => {
    if (!period) {
        return startTime + getToday()*23*hours
    } else {
        return startTime + period * 23 * hours;
    }
};

const priceOnPeriod = module.exports.priceOnPeriod = async (period) => {
    let total = await getTotal(period);
    return total.dividedBy(eosSupply).floor();
};

const getTotal = module.exports.getTotal = async(period) => {
    return await EOSSale.dailyTotals({
        period: period,
        walletAddress:botCfg.account
    });
};

Object.defineProperty(module.exports, "gasPrice", {
    get: async () => {
        let gasPrice = await EOSSale.eth.getGasPrice()*config.bot.gasPriceTimes || config.bot.gasPriceMin;
        if(gasPrice < config.bot.gasPriceMin){
            gasPrice = config.bot.gasPriceMin;
        }
        if(gasPrice > config.bot.gasPriceMax) {
            gasPrice = config.bot.gasPriceMax;
        }
        return gasPrice;
    }
});

module.exports.currentPrice = async() => {
    return await priceOnPeriod(getToday());
};


//Функция для перевода гвей в эфир
const toETH = module.exports.toETH = (wei) => {
    let divide = new BigNumber(EOSSale.utils.unitMap["ether"]);
    return new BigNumber(wei).dividedBy(divide);
};

module.exports.fromETH = (col) => {
    let times = new BigNumber(EOSSale.utils.unitMap["ether"]);
    return new BigNumber(col).times(times);
};

module.exports.optimalETH = (z, y, s) => {
    //Рассчитываем оптимальное количество эфира для входа
    //Формула x = Корень(z*y*s) - z
    //y - текущая цена на биржах
    //s - кол-во токенов
    //z - сколько уже вкинули в период
    z = toETH(z);
    let zys = new BigNumber(z*toETH(y)*s.toString());
    let _sqrt = zys.sqrt();
    return _sqrt.minus(z);
};

module.exports._value = (z, y, s, x, fee) => {
    //Расчитываем велью по формуле:
    //f(x) = (sxy - xz - x2)/(z+x) - fee
    //Где x - количество эфира для входа
    //z - сколько закинули
    //y - текущая цена на биржах
    //s - кол-во токенов
    //fee - размер затрат на совершение всех операций
    let sxy = x.times(s).times(toETH(y));
    let xz = x.times(toETH(z));
    let x2 = x.times(x);

    let first = sxy.minus(xz).minus(x2);
    let z_x = x.plus(toETH(z));

    let second = first.dividedBy(z_x);
    return second.minus(toETH(fee));
};






const BigNumber = require('bignumber.js');
const BuyLogicHelper = require('./BuyLogicHelper');
const BinanceREST = require('../../exchangesAPI/BinanceREST');
const Token = require('../../blockchain/web3/token');
const botCfg = require('../../config').bot;
const TransactionStatus = require('../../resources/TransactionStatus');
BigNumber.config({ ERRORS: false });

const EOS_MIN_ETH = 0.01;
const eosSupply = new BigNumber(2000000); // Количество токенов продаваемых за раунд
const EventEmitter = require('events');


class BuyLogic extends EventEmitter {
    constructor() {
        super();
        this.contract = require('../../blockchain/web3/Tokens/EOSSale');
        this.eosContract = new Token('EOS');
    }

    async excDelta(period){
        let currentPrice = period ? await BuyLogicHelper.priceOnPeriod(period):await BuyLogicHelper.currentPrice();
        let exchangePrice = await BinanceREST.lastPrice('EOSETH');
        exchangePrice = new BigNumber(this.contract.utils.toWei(exchangePrice.toString(), "ether"));
        this.emit("logger.debug", 'Цена на бирже: ' + BuyLogicHelper.toETH(exchangePrice) + '; Текущая цена: ' + BuyLogicHelper.toETH(currentPrice));

        if(currentPrice.lessThan(exchangePrice)){
            return { exchangePrice: exchangePrice };
        }else return false;
    }

    //Расчитываем сколько нам надо вкинуть эфира
    //Рассчитываем доходность, если все ок, то проводим транзакцию
    async calculate(exchangePrice, period) {
        //Рассчитываем оптимальное кол-во эфира для входа
        if(!period) period = BuyLogicHelper.getToday();

        //Сколько эфира уже вкинули?
        let ethSubmitted = await BuyLogicHelper.getTotal(period);

        this.emit("logger.debug", 'Уже закинули в период ' + BuyLogicHelper.toETH(ethSubmitted));

        if (ethSubmitted >= 0) {

            let optimalETH = BuyLogicHelper.optimalETH(ethSubmitted, exchangePrice, eosSupply);
            if (optimalETH.lessThan(0)) {
                this.emit("logger.debug", `!!! Отрицательное значение оптимального ETH: ${optimalETH} !!!`);
                return;
            }
            this.emit("logger.debug", 'Оптимальное количество эфира ' + optimalETH);

            //
            //Получаем рекомендуемую стоимость газа из оракла блокчейна
            let gasPrice = await BuyLogicHelper.gasPrice;
            this.emit("logger.debug", "Рекомендуемая цена газа: " + gasPrice);

            //Сколько мы потратим на совершение тразнакции?
            let fee = new BigNumber ((botCfg.GasLimitRound + botCfg.GasLimitClaim + botCfg.GasLimitExchange) * gasPrice);
            this.emit("logger.debug", 'Потратим на коммисиях: ' + BuyLogicHelper.toETH(fee));


            let balance = new BigNumber(await this.contract.eth.getBalance(botCfg.account));
            balance.times("90").dividedBy("100"); // Мы не берем полный баланс с расчетом то что вдруг мы можем тупо обнулится и транзакция не пройдет в конечном итоге
            if (BuyLogicHelper.toETH(balance).lessThan(optimalETH)) {
                optimalETH = BuyLogicHelper.toETH(balance.minus(fee));
                this.emit("logger.debug", 'Баланс аккаунта не позволяет войти на рассчитанную сумму. Входим на ' + optimalETH);
            }
            this.emit("logger.debug", 'Баланс аккаунта ' + BuyLogicHelper.toETH(balance));
            if(optimalETH.lessThan(EOS_MIN_ETH)) {
                this.emit("logger.debug", `Calculated ${optimalETH} is less than ${EOS_MIN_ETH} ETH, which is minimal amount to be in crowdsale`);
                return false;
            }

            //Исходим из того, что кроме нас еще будут вкидывать эфир и уменьшаем свою долю
            //optimalETH.times(self.config.discount);

            if (botCfg.max && optimalETH > botCfg.max) {
                optimalETH = botCfg.max;
                this.emit("logger.debug", 'Оптимальное количество эфира больше заданного максимального значения: ' + optimalETH);
            }

            let value = BuyLogicHelper._value(ethSubmitted, exchangePrice, eosSupply, optimalETH, fee);
            this.emit("logger.debug", "Ожидаемая доходность: " + value + " - " + value.dividedBy(optimalETH).times(new BigNumber(100)) + "%");

            if (value.greaterThan(0)) return {
                deltaValue: value,
                optimalETH:optimalETH, //
                gasPrice:gasPrice,
                delta: value.dividedBy(optimalETH).times(new BigNumber(100)).toString(),
                period:period,
                buyPrice:BuyLogicHelper.toETH(exchangePrice).toString(),
            }; else {
                this.emit("logger.debug", 'Пропускаем период - отрицательная или нулевая доходность');
                return false;
            }
        }
    }

    async BuyTokens({optimalETH, gasPrice, gasLimit }) {
        return new Promise( async () => {
            let buyResponse = await this.contract.buyTokens({
                amount: optimalETH, // BigNumber
                gasPrice: gasPrice,
                gasLimit: gasLimit,
                privateKey: botCfg.privateKey,
            });

            buyResponse.on(TransactionStatus.GENERATED, async (hash) => {
                this.emit("logger.debug", "Транзакция отправлена: " + hash);
            });

            buyResponse.on(TransactionStatus.NOTGENERATED, (err) => {
                this.emit("logger.debug", "Ошибка при отправке транзакции: " + err);
            });
        })
    }

    claimPeriod({period, gasPrice, gasLimit}){
        return new Promise( async (resolve) => {
            resolve(await this.contract.claim({
                period: period,
                gasPrice: gasPrice, // gcClaim
                gasLimit: gasLimit,
                privateKey: botCfg.privateKey,
            }));
        });
    }
}

module.exports = new BuyLogic;
const fs = require('fs');
const EventEmitter = require('events');

class TaskList extends EventEmitter{
    constructor(file) {
        super();
        this.data = [];
        this.file = file;
        this._load();
    }

    add({ hash, price, side, symbol }){
        this.data[hash] = {
            price: price,
            side: side,
            symbol: symbol,
            quantity:0,
            made: false,
            orderId:0,
        };
        this.emit("logger.debug", "Обнаружена новая задача на выполнение, данные:");
        this.emit("logger.debug", "hash: "+hash+"; price: "+price+"; side: "+side+"; symbol: "+symbol);
        this._save();
    }

    setProp(hash, prop, value){
        if(!this.data.hasOwnProperty(hash)) return;
        this.data[hash][prop] = value;
        this._save();
    }

    get(hash){
        if(!this.data.hasOwnProperty(hash)) return undefined;
        return this.data[hash];
    }

    getProp(hash, prop){
        if(!this.data.hasOwnProperty(hash)) return undefined;
        return this.data[hash][prop];
    }

    has(hash){
        return this.data.hasOwnProperty(hash) && !this.data[hash]["made"];
    }

    _load(){
        const saveData = JSON.parse(fs.readFileSync(this.file, 'utf8'));
        this.data = {};
        for(let hash in saveData){
            if(!saveData.hasOwnProperty(hash)) continue;
            this.data[hash] = {
                price: saveData[hash]["price"],
                side: saveData[hash]['side'],
                symbol: saveData[hash]['symbol'],
                quantity: saveData[hash]['quantity'],
                made: saveData[hash]['made'],
                orderId:saveData[hash]['orderId'],
            }
        }
    }

    _save(){
        const saveData = {};
        for(let hash in this.data){
            if(!this.data.hasOwnProperty(hash)) continue;
            saveData[hash] = {
                price: this.data[hash]["price"],
                side: this.data[hash]['side'],
                symbol: this.data[hash]['symbol'],
                quantity: this.data[hash]['quantity'],
                made: this.data[hash]['made'],
                orderId:this.data[hash]['orderId'],
            };
        }
        fs.writeFileSync(this.file, JSON.stringify(this.data));
    }
}

module.exports = TaskList;
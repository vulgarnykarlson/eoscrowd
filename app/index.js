const BuyLogic = require('./components/BuyLogic');
const BuyLogicHelper = require('./components/BuyLogicHelper');
const config = require('../config');
const unix = require('../libs/unix');
const TransactionStatus = require('../resources/TransactionStatus');
const TaskManager = require('./components/TaskManager');
const BinanceREST = require('../exchangesAPI/BinanceREST');
const BigNumber = require('bignumber.js');
const EventEmitter = require('events');
const Token = require('../blockchain/web3/token');
const PeriodList = require('./PeriodList');
class App extends EventEmitter {
    constructor(periodFile = __dirname+'/period.json'){
        super();
        BuyLogic.on("logger.debug", (msg) => this.emit("logger.debug", "BL "+msg));
        TaskManager.tasks.on("logger.debug", (msg) => this.emit("logger.debug", "TaskList "+msg));
        TaskManager.on("logger.debug", (msg) => this.emit("logger.debug", "TaskManager "+msg));
        this.periodList = new PeriodList(periodFile);
        this.eosContract = new Token('EOS');
    };

    async startBuy(periodNum = BuyLogicHelper.getToday()){
        const timeToBuy = BuyLogicHelper.periodEnd(periodNum) - unix.now;
        this.periodList.initialize(periodNum); //  Инициализация стандартных параметров

        if(this.periodList.getProp(periodNum, "closed")) return; // Заглушка в случай когда в период не была совершена покупка
        // Отправка на клайм после покупки
        if(this.periodList.getProp(periodNum, "buyed") && !this.periodList.getProp(periodNum, "claimed") && timeToBuy < 2){
            this.periodList.setProp(periodNum, "claimed", true);
            setTimeout(this.claimPeriod.bind(this), (timeToBuy+config.bot.claimTime+2)*1000); // Запускаем клайм
        }

        // Если не произошла покупка и время до покупки осталось меньше чем задано в конфиге то выполнить дальнейшие условия
        if(!(!this.periodList.getProp(periodNum, "buyed") && timeToBuy < config.bot.blockTime && timeToBuy)){
            const timeStart = timeToBuy-config.bot.blockTime;
            if(timeStart % 30 === 0 && timeStart < 300) this.emit("logger.debug", "Период #"+periodNum+" время до покупки осталось "+unix.timeLeft(timeStart));
            return true;
        }
        this.periodList.setProp(periodNum, "buyed", true);

        this.emit("logger.debug", "=== РАСЧЕТ ВОЗМОЖНОСТИ ВХОДА === Период "+periodNum);
        this.emit("logger.debug", "Уже куплено в период #"+periodNum+" на "+this.periodList.getProp(periodNum, "buyedETH").toFixed(5) + " эфира");
        let isPossibleEnter = await BuyLogic.excDelta(periodNum);

        if(!isPossibleEnter) {
            this.periodList.setProp(periodNum, "claimed", true);
            this.emit("logger.debug", "Вход невыгоден!");
            this.periodList.setProp(periodNum, "closed", true);
            return;
        }
        isPossibleEnter = await BuyLogic.calculate(isPossibleEnter.exchangePrice, periodNum);
        if(!isPossibleEnter) {
            this.periodList.setProp(periodNum, "closed", true);
            return;
        }
        let { optimalETH: optimalETH, gasPrice:gasPrice }  = isPossibleEnter;

        gasPrice = await BuyLogicHelper.gasPrice;

        this.periodList.incProp(periodNum, "buyedETH", optimalETH);
        this.periodList.incProp(periodNum, "fee", gasPrice*config.bot.GasLimitRound);

        let buyResponse = await BuyLogic.BuyTokens({
            optimalETH: optimalETH,
            gasPrice: gasPrice,
            gasLimit: config.bot.GasLimitRound
        });

        buyResponse.on(TransactionStatus.GENERATED, (hash) => {

            this.emit("logger.debug", "Покупка токенов период #"+periodNum+" hash "+hash);
            this.emit("logger.debug", " == Закончили цикл покупки, далее клайм а после отправка на биржу == ")
        });
    }

    async claimPeriod(periodNum = BuyLogicHelper.getToday()-1){
        this.emit("logger.debug", "Клайм период #"+periodNum);
        let gasPrice = await BuyLogicHelper.gasPrice;
        this.periodList.incProp(periodNum, "fee", gasPrice*config.bot.GasLimitClaim);
        let tx = await BuyLogic.claimPeriod({
            period:periodNum,
            gasLimit: config.bot.GasLimitClaim,
            gasPrice: gasPrice,
        });

        tx.on(TransactionStatus.GENERATED, (hash) => {
            this.emit("logger.debug", "Клайм,  период #"+periodNum+" hash "+hash);
        });
    }

    async balanceChecker(periodNum = (BuyLogicHelper.getToday()-1)){
        if(!(this.periodList.getProp(periodNum, "buyed")
                && this.periodList.getProp(periodNum, "claimed")
                && !this.periodList.getProp(periodNum, "sentExchange")
                && !this.periodList.getProp(periodNum, "closed")
            )
          ) return;
        this.periodList.setProp(periodNum, "sentExchange", true);
        // проверяем, есть ли клайм, если клайм не совершился, откатываем обратно и по циклу
        let tokens = await this.eosContract.getBalance(config.bot.account);
        if(tokens.lessThan(0.5)){
            this.emit("logger.debug", "Ожидание окончания клайм периода");
            this.periodList.setProp(periodNum, "sentExchange", false);
            return;
        }
        this.emit("logger.debug", "== Расчет реальных цен и отпрака на биржу период #"+periodNum);
        let priceForToken = await BuyLogicHelper.priceOnPeriod(periodNum);
        let gasPrice = await BuyLogicHelper.gasPrice;
        this.periodList.incProp(periodNum, "fee", gasPrice*config.bot.GasLimitExchange);
        const fee = this.periodList.getProp(periodNum, "fee");
        this.emit("logger.debug", `On period ${periodNum} we received ${tokens} EOS`);

        this.emit("logger.debug", "Real fee for all transactions " + BuyLogicHelper.toETH(fee));
        this.emit("logger.debug", "Real paid price for one token was (without fee) - " + BuyLogicHelper.toETH(priceForToken).toFixed(5));

        this.periodList.setProp(periodNum, "buyPrice", BuyLogicHelper.toETH(priceForToken).toFixed(5));

        const sellPrice = this.periodList.getProp(periodNum, "buyedETH").times('1000000000000000000').plus(fee).dividedBy(tokens).times(parseFloat(config.bot.delta.toString())/100 + 1);
        this.emit("logger.debug", "Sell price for one token with takeprofit " + config.bot.delta+"% - "+ sellPrice.toFixed(5));
        this.periodList.setProp(periodNum, "sellPrice", sellPrice.toFixed(5));
        this.emit("logger.debug", '=== Отсылаем деньги на биржу ===');
        let res = await BinanceREST.client.depositAddress({asset:'EOS'});
        if(!res["success"]) return;

        let tx = await this.eosContract.sendTokens({
            privateKey: config.bot.privateKey,
            to: res.address,
            gasLimit: config.bot.GasLimitExchange,
            gasPrice: gasPrice,
            amount: tokens
        });


        tx.on(TransactionStatus.GENERATED, (hash) => {
            this.periodList.setProp(periodNum, "hash", hash);
            this.emit("logger.debug", '=== Hash транзакции при отсылке на биржу :'+hash+' ===');
            TaskManager.tasks.add({
                hash: hash,
                side: 'SELL',
                symbol: 'EOSETH',
                price: sellPrice.toFixed(5),
            });
        });
        this.periodList.setProp(periodNum, "closed", true);
    }

    start(){
        setInterval(this.startBuy.bind(this),1000);
        setInterval(this.balanceChecker.bind(this), 5000);
    }
}

module.exports = App;
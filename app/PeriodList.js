const BigNumber = require('bignumber.js');
const fs = require('fs');
// todo - не записывает в файл параметры при incProp
class PeriodList {
    constructor(file) {
       this.data = {};
       this.file = file;
       this._load();
    }

    initialize(period){
        if(this.data.hasOwnProperty(period)) return;
        this.data[period] = {
            buyed: false,
            sentExchange: false,
            claimed: false,
            closed: false,
            buyedETH: new BigNumber(0),
            buyPrice: new BigNumber(0),
            sellPrice: new BigNumber(0),
            gasPrice: new BigNumber(0),
            fee: new BigNumber(0),
            hash: '',
        };
        this._save();
    }

    setProp(period, prop, value){
        if(!this.data.hasOwnProperty(period)) return;
        this.data[period][prop] = value;
        this._save();
    }

    incProp(period, prop, value){
        if(!this.data.hasOwnProperty(period)) return;
        this.data[period][prop] = this.data[period][prop].plus(value);
        this._save();
    }

    getProp(period, prop){
        if(!this.data.hasOwnProperty(period)) return undefined;
        return this.data[period][prop];
    };

    _load(){
        const saveData = JSON.parse(fs.readFileSync(this.file, 'utf8'));
        this.data = {};
        for(let period in saveData){
            if(!saveData.hasOwnProperty(period)) continue;
            this.data[period] = {
                buyed: saveData[period].buyed,
                claimed: saveData[period].claimed,
                closed: saveData[period].closed,
                sentExchange: saveData[period].sentExchange,
                buyedETH: new BigNumber(saveData[period].buyedETH),
                buyPrice: new BigNumber(saveData[period].buyPrice),
                sellPrice: new BigNumber(saveData[period].sellPrice),
                fee: new BigNumber(saveData[period].fee),
                hash: saveData[period].hash
            }
        }
    }

    _save(){
        const saveData = {};
        for(let period in this.data){
            //if(!this.data.hasOwnProperty(period)) continue;
            saveData[period] = {
                buyed: this.data[period].buyed,
                claimed: this.data[period].claimed,
                closed: this.data[period].closed,
                sentExchange: this.data[period].sentExchange,
                buyedETH: this.data[period].buyedETH.toString(),
                buyPrice: this.data[period].buyPrice.toString(),
                sellPrice: this.data[period].sellPrice.toString(),
                fee: this.data[period].fee.toString(),
                hash: this.data[period].hash
            }
        }
        fs.writeFileSync(this.file, JSON.stringify(this.data));
    }
}

module.exports = PeriodList;
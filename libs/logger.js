let Winston = require("winston");
let logFile = 'debugs.log';
module.exports.setFile = (logName) => {
    logFile = logName;
    Object.assign(this, new (Winston.Logger)({
        transports: [
            new (Winston.transports.Console)({
                colorize: true,
                timestamp: true,
                level: "debug"
            }),
            new (Winston.transports.File)({
                level: "info",
                timestamp: true,
                filename: logFile,
                json: false
            })
        ]
    }));
};

module.exports.info = this.info;
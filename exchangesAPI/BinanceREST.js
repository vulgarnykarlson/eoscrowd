const Binance = require('binance-api-node');
const cfg = require('../config').exchanges.binance;
class BinanceREST {
    constructor(){
        this.client = Binance.default({
            apiKey: cfg.apiKey,
            apiSecret: cfg.apiSecret
        });
    };

    //Последняя цена для конкретной пары
    async lastPrice(symbol) {
        var price = await this.client.prices({symbol: symbol});
        return price[symbol];
    };

    async getBestPrice({ symbol:symbol, timestamp:timestamp }){
        timestamp = timestamp*1000;
        let res = await this.client.aggTrades({
            symbol: symbol,
            startTime: timestamp-30000,
            endTime: timestamp-1000
        });
        return Math.max.apply(Math,res.map(function(o){return parseFloat(o.price);}));
    }

    async getSellPrice({ symbol:symbol, timestamp:timestamp }){
        timestamp = timestamp*1000;
        let res = await this.client.aggTrades({
            symbol: symbol,
            startTime: timestamp+180000,
            endTime: timestamp+3770000,
        });
        return Math.max.apply(Math,res.map(function(o){return parseFloat(o.price);}));
    }
}

module.exports = new BinanceREST;
const Token = require('../token');
const BigNumber = require('bignumber.js');
BigNumber.config({ ERRORS: false });

class EOSSale extends Token{
    constructor(){
        super('EOSSALE');
    }

    dailyTotals({ period }) {

        return new Promise( (resolve) => {
            this.tokenContract.methods.dailyTotals(period).call({}, (err, res) => {
                if (err) resolve({ err:'Не удалось считать кол-во эфира закинутого в период: ' + err });
                if (res) resolve(new BigNumber(res));
                else resolve({ err:'RESULT OF DAILY TOTALS IS EMPTY'});
            });
        })
    }

    async claimAll({ gasPrice, gasLimit, privateKey}){
        let data = this.tokenContract.methods["claimAll"]().encodeABI();
        return await this.send({
            privateKey:privateKey,
            gasLimit: gasLimit,
            gasPrice: gasPrice,
            amount:0,
            to:this.tokenAddress,
            data:data,
            confirmationCount: 0,
        });
    }

    async claim({ period, gasPrice, gasLimit, privateKey}){
        let data = this.tokenContract.methods["claim"](period).encodeABI();
        return await this.send({
            privateKey:privateKey,
            gasLimit: gasLimit,
            gasPrice: gasPrice,
            amount:0,
            to:this.tokenAddress,
            data:data,
            confirmationCount: 0,
        });
    }

    async buyTokens({ amount, gasPrice, gasLimit, privateKey }){
        return await this.send({
            privateKey: privateKey,
            to: this.tokenAddress,
            amount: amount,
            gasLimit: gasLimit,
            gasPrice: gasPrice,
        });
    }
}

module.exports = new EOSSale;
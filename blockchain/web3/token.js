const Web3Extended = require('./index.js');
const TokenAddresses = require('../../resources/TokenAddresses.js');
const TokenABI = require('../../resources/TokenABI.js');
const BigNumber = require('bignumber.js');

class Token extends Web3Extended {
    constructor(name){
        super();
        name = name.toUpperCase();
        if(!TokenAddresses[name]) throw new Error(`!!! ${name} uknown token address`);
        if(!TokenABI[name]) throw new Error(`${name} uknown token abi`);
        this.tokenName = name;
        this.tokenAddress   = TokenAddresses[name];
        this.tokenContract  = new this.eth.Contract(TokenABI[name], TokenAddresses[name]);

        if(!this.tokenContract)  throw new Error(`!!! ${this.tokenName} TOKEN CONTRACT INIT FAILED !!!`);
    }

    async getBalance(walletAddress){
        if(!this.utils.isAddress(walletAddress)) new Error('NO ACCOUNT IS KNOWN');
        return new BigNumber(await this.tokenContract.methods["balanceOf"](walletAddress).call());
    }

    async sendTokens({ privateKey, to, gasLimit, gasPrice, amount, confirmationCount}){
        if(!this.eth.accounts.privateKeyToAccount(privateKey).address)
            return { err: 'Probably wrong private key was entered. Transactions will be depricated '};

        return await this.send({
            privateKey: privateKey,
            to: this.tokenAddress,
            gasLimit: gasLimit,
            gasPrice: gasPrice,
            confirmationCount: confirmationCount,
            data: this.tokenContract.methods["transfer"](to, this.utils.toHex(amount.toString())).encodeABI(),
        });
    }
}

module.exports = Token;
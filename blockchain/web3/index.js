const Web3 = require('web3');
const TransactionStatus = require('../../resources/TransactionStatus');
const EventEmitter = require('events');

//const http_provide = 'http://localhost:8545';
const http_provide = 'https://mainnet.infura.io/atBop6XSlyWjZhZp7gSP';

class Web3Extended extends Web3 {
    constructor() {
        super(new Web3.providers.HttpProvider(http_provide));
    }

    async send({ privateKey, to, gasLimit, gasPrice, amount, data, account, confirmationCount = 1}){
        return new Promise( async (resolve) => {
            let tx = await this.eth.accounts.signTransaction({
                to, gasLimit, gasPrice,
                chainId:1,
                data:data || "",
                value: amount ? this.utils.toWei(amount.toString(), "ether") : 0,
            }, privateKey);
            if (!tx["rawTransaction"]) resolve({ err: "Transaction raw is wrong" });

            let res = new EventEmitter();
            let response = this.eth.sendSignedTransaction(tx["rawTransaction"]);
            response.on('transactionHash', (hash) => {
                res.emit(TransactionStatus.GENERATED, hash);
            });
            response.on('error', (err) => {
                res.emit(TransactionStatus.NOTGENERATED, err)
            });
            response.on('confirmation', (confirmationNumber) => {
                res.emit(TransactionStatus.NEWCONFIRMATION, confirmationNumber);
                if (confirmationNumber === confirmationCount) {
                    res.emit(TransactionStatus.CONFIRMED)
                }
            });
            resolve(res);
        })
    }
}

module.exports = Web3Extended;
const App = require('./app/index');
let app = new App;
let logger = require('./libs/logger');
logger.setFile(__dirname+"/debugs.log");
app.on("logger.debug", logger.info);
( async() => {
    await app.start();
    logger.info("Belsona EOS Crowdsale exchanger started");
})();
